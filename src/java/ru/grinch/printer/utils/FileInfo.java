package ru.grinch.printer.utils;

import java.io.File;
import java.io.IOException;

public class FileInfo{

	public void printInfoFile(String fileName){


				File directory = new File(fileName);

				if(directory.isDirectory()) {

					for(File i : directory.listFiles()) {

						if(i.isDirectory()) {
							System.out.println(i.getName() + " directory");
						}

						else {
							System.out.println(i.getName() + " " + i.length());
						}
					}
				}
	}
}