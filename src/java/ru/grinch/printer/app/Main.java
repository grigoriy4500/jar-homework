package ru.grinch.printer.app;

import ru.grinch.printer.utils.FileInfo;
import com.beust.jcommander.JCommander;

class Main{
	public static void main(String[] args) {

		Arguments arguments = new Arguments();

		JCommander.newBuilder()
			.addObject(arguments)
			.build()
			.parse(args);

		FileInfo file = new FileInfo();
		file.printInfoFile(arguments.fileNames);
	}
}