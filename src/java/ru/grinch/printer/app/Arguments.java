package ru.grinch.printer.app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
class Arguments{

	@Parameter(names = {"--fileNames"})
	public String fileNames;

}